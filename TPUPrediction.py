import tensorflow as tf
print(f"Tensorflow version: {tf.__version__}")
import numpy as np
from absl import app, flags, logging
from absl.flags import FLAGS
from yolov3_tf2.models import YoloV3
from yolov3_tf2.utils import draw_outputs
from yolov3_tf2.dataset import transform_images
import cv2

TPU_CORES = 8

flags.DEFINE_string('classes', './data/coco.names', 'path to classes file')
flags.DEFINE_string('weights', './checkpoints/yolov3.tf', 'path to weights file')

flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_string('image', './test/frame_3510.jpg', 'path to input image')
flags.DEFINE_string('output', './output_frame.jpg', 'path to output image')
flags.DEFINE_integer('num_classes', 3, 'number of classes in the model')
flags.DEFINE_string('gcs_pattern', './tfrecords/*.tfrec', 'TFRecord files pattern')

flags.DEFINE_integer('batch_size', 128*TPU_CORES, 'TPU number of cores')
flags.DEFINE_string('model_dir', 'PATH/TO/MODEL_DIR', 'Model checkpoint dir')

def input_fn(params):
    def parse_tfrecord(serialized_example, size=416):
        feature_description = {
            'label': tf.io.FixedLenFeature([], tf.int64),
            'image_raw': tf.io.FixedLenFeature([], tf.string)
        }
        
        feature = tf.io.parse_single_example(serialized_example, feature_description)

        label = feature['label']

        img_raw = tf.image.decode_jpeg(feature['image_raw'], channels=3)
        img_raw = tf.image.resize(img_raw, (size, size))

        return img_raw, label

    filenames = tf.io.gfile.glob(FLAGS.gcs_pattern) 
    AUTO = tf.data.experimental.AUTOTUNE
    dataset = tf.data.TFRecordDataset(filenames, num_parallel_reads=AUTO)

    options = tf.data.Options()
    options.experimental_deterministic = False
    dataset.with_options(options)

    parsed_dataset = dataset.map(lambda element: parse_tfrecord(element, FLAGS.size))
    batched_dataset = parsed_dataset.batch(params['batch_size'])
    return batched_dataset

def model_fn(features, labels, mode, config, params):
    if mode == tf.estimator.ModeKeys.PREDICT:
        yolo = YoloV3(size=FLAGS.size, classes=FLAGS.num_classes)
        boxes, scores, classes, nums = yolo.predict(features, steps=1)

        return tf.compat.v1.estimator.tpu.TPUEstimatorSpec(
            mode=tf.estimator.ModeKeys.PREDICT, 
            predictions={
                'boxes': boxes,
                'scores': scores,
                'classes': classes,
                'nums': nums
            },
        )

def run_prediction_tpu(_argv):
    tpu_cluster_resolver = tf.distribute.cluster_resolver.TPUClusterResolver(
        tpu=FLAGS.tpu_name, zone=FLAGS.zone, project=FLAGS.gcs_project
    )
    run_config = tf.compat.v1.estimator.tpu.RunConfig(
        cluster = tpu_cluster_resolver,
    )
    estimator = tf.compat.v1.estimator.tpu.TPUEstimator(
        model_fn = model_fn,
        model_dir = FLAGS.model_dir,
        use_tpu = True,
        train_batch_size = 1024,
        predict_batch_size = FLAGS.batch_size,
        config = run_config,
    )
    results = estimator.predict(input_fn=input_fn)
    r = next(results)
    print(r)
    

if __name__ == '__main__':
    try:
        app.run(run_prediction_tpu)
    except SystemExit:
        pass