import tensorflow as tf
print(f"Tensorflow version: {tf.__version__}")
import numpy as np
from absl import app, flags, logging
from absl.flags import FLAGS
from yolov3_tf2.models import YoloV3
from yolov3_tf2.utils import draw_outputs
import cv2
import time

# TPU configs
TPU_NAME = ''
ZONE = ''
GCS_PROJECT = ''

# Connect to TPU Hardware
tpu_cluster_resolver = tf.distribute.cluster_resolver.TPUClusterResolver(tpu=TPU_NAME, zone=ZONE, project=GCS_PROJECT)
tf.config.experimental_connect_to_cluster(tpu_cluster_resolver)
tf.tpu.experimental.initialize_tpu_system(tpu_cluster_resolver)
strategy = tf.distribute.TPUStrategy(tpu_cluster_resolver)
AUTO = tf.data.experimental.AUTOTUNE

# Used in dataset options
options = tf.data.Options()
options.experimental_deterministic = False
print("All devices: ", tf.config.list_logical_devices('TPU'))
print("REPLICAS: ", strategy.num_replicas_in_sync)

# FLags 
flags.DEFINE_string('classes', './data/coco.names', 'path to classes file')
flags.DEFINE_string('weights', 'gs://path/to/weights.tf', 'path to weights file')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_integer('num_classes', 3, 'number of classes in the model')
flags.DEFINE_string('gcs_pattern', 'gs://pattern/to/*.tfrec', 'TFRecord files pattern')
flags.DEFINE_string('model_dir', 'gs://path/to/checkpoints/dir', 'Model checkpoint dir')
flags.DEFINE_integer('batch_size', 8, 'Input batch size')


def decode_image(image_data, size=416):
    image = tf.image.decode_jpeg(image_data, channels=3)
    image = tf.image.resize(image, (size, size))
    image = tf.math.divide(image, 255)

    return image


def parse_tfrecord(serialized_example):
    feature_description = {
        'image_raw': tf.io.FixedLenFeature([], tf.string),
        'label': tf.io.FixedLenFeature([], tf.int64),
    }
    feature = tf.io.parse_single_example(serialized_example, feature_description)

    label = feature['label']
    image = decode_image(feature['image_raw'], size=FLAGS.size)

    return image, label


def load_dataset(pattern, ordered=False):
    filenames = tf.io.gfile.glob(pattern) 

    dataset = tf.data.TFRecordDataset(filenames, num_parallel_reads=AUTO)
    if not ordered:
        dataset.with_options(options)

    parsed_dataset = dataset.map(parse_tfrecord)
    return parsed_dataset


def get_predicion_dataset(ordered=False, divide_in_batches=True):
    dataset = load_dataset(pattern=FLAGS.gcs_pattern, ordered=ordered)
    if divide_in_batches:
        dataset = dataset.batch(FLAGS.batch_size, drop_remainder=True)
    dataset = dataset.prefetch(AUTO)

    return dataset

def run(_argv):
    prediction_dataset = get_predicion_dataset(ordered=False, divide_in_batches=True)
    
    class_names = [c.strip() for c in open(FLAGS.classes).readlines()]

    with strategy.scope():
        yolo_model = YoloV3(size=FLAGS.size, classes=FLAGS.num_classes)
        yolo_model.load_weights(FLAGS.weights)

    start = time.time()
    boxes, scores, classes, nums = yolo_model.predict(prediction_dataset, verbose=1)
    end = time.time()
    print(f">>>>>> ELAPSED TIME: {end-start}")


if __name__ == '__main__':
    try:
        app.run(run)
    except SystemExit:
        pass